import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from '../interface/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private url: string = 'http://????/';

  constructor(private http: HttpClient) { }

  findAll(): Observable <Contact[]>{
    return this.http.get<Contact[]>(this.url);
  }

  findByContact(): Observable <Contact>{
    return this.http.get<Contact>(`${this.url}/contact`)
  }

  add(contact : Contact): Observable<Contact>{
return this.http.post<Contact>(this.url, contact);
  }

  find(id: number): Observable<Contact>{
    return this.http.get<Contact>(`${this.url}${id}`)
  }
}
