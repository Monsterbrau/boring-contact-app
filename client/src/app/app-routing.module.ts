import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { ContactComponent } from "./contact/contact.component";



const routes: Routes = [
    { path: "", redirectTo: "/contact", pathMatch: "full" },
    { path: "contact", component: ContactComponent },

];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }